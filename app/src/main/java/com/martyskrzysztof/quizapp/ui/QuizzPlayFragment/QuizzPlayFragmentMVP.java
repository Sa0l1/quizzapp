package com.martyskrzysztof.quizapp.ui.QuizzPlayFragment;

/**
 * Created by krzysztofm on 15.03.2018.
 */

public interface QuizzPlayFragmentMVP {
    interface Presenter {
        void loadQuestionsData(String mQuizIdString, int question);
    }

    interface View {
        void openFinishFragment(String mQuizIdString, String mQuizScore);

        void setupRadioButton(int i1, String text);

        void setProgressBarMaxSize(int questionsCount);

        void setProgressBarProgress(int i);

        void setQuizPlayName(String title);

        void setQuizQuestionName(String text);

        void setQuizImage(String url);

        int returnUserAnswerListSize();

        int returnUserAnswerListValues(int i);
    }
}
