package com.martyskrzysztof.quizapp.ui.SingleQuizzActivity;

import android.support.v4.app.Fragment;

import java.util.Date;

/**
 * Created by krzysztofm on 13.03.2018.
 */

interface SingleQuizzActivityMVP {
    interface View {
        void singleQuizzActivityBackPressed();

        void setMainPhotoUrl(String mainPhoto);

        void setTitle(String quizTitleString);

        void setCreatedAt(Date quizCreatedAtString);

        void setType(String quizTypeString);

        void setContent(String quizContentString);

        void openQuizzPlayFragment();
    }

    interface Presenter {
        void backPressedAlert();

        void loadSingleQuizzInfo(String SingleQuizzId);
    }
}
