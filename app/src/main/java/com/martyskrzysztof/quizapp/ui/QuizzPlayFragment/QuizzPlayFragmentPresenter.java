package com.martyskrzysztof.quizapp.ui.QuizzPlayFragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.martyskrzysztof.quizapp.App;
import com.martyskrzysztof.quizapp.utils.network.RestAPI;
import com.martyskrzysztof.quizapp.utils.network.jsonResponses.SingleQuizJsonResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by krzysztofm on 15.03.2018.
 */

public class QuizzPlayFragmentPresenter implements QuizzPlayFragmentMVP.Presenter {

    private static final String TAG = "QuizzPlayFragmentPresen";

    @Inject
    Retrofit retrofit;

    private RadioGroup radioGroup;
    private RadioButton[] radioButton;

    private ArrayList<Integer> correctAnswerList;
    private ArrayList<Integer> userAnswerList;

    private int questionsCount;
    private Integer answersCount;
    private Integer correctAnswerId = 0;

    private int i1 = 0;
    private int ansId;

    private Context context;
    private QuizzPlayFragmentMVP.View view;
    private int mQuizScore = 0;

    public QuizzPlayFragmentPresenter(Context context, QuizzPlayFragmentMVP.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void loadQuestionsData(final String mQuizIdString, final int question) {
        ((App) context.getApplicationContext()).getNetComponent().inject(this);
        correctAnswerList = new ArrayList<>();
        userAnswerList = new ArrayList<>();

        RestAPI restApi = retrofit.create(RestAPI.class);
        Call<SingleQuizJsonResponse> call = restApi.getSingleQuiz(mQuizIdString);
        call.enqueue(new Callback<SingleQuizJsonResponse>() {
            @Override
            public void onResponse(Call<SingleQuizJsonResponse> call, Response<SingleQuizJsonResponse> response) {
                questionsCount = response.body().getQuestions().size();
                view.setProgressBarMaxSize(questionsCount);
                view.setProgressBarProgress(question);

                if (question >= questionsCount) {

                    Log.e(TAG, "onResponse: size: " + correctAnswerList.size());
                    Log.e(TAG, "onResponse: size: " + userAnswerList.size());
                    for (int l = 0; l < userAnswerList.size(); l++) {
                        try {
                            if (userAnswerList.get(l).equals(correctAnswerList.get(l))) {
                                mQuizScore++;
                                Log.d(TAG, "onResponse: mQuizScore: " + mQuizScore);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                    Log.e(TAG, "onResponse: mQuizScore: " + mQuizScore);
                    Log.e(TAG, "onResponse: mQuizIdString: " + mQuizIdString);
                    view.openFinishFragment(mQuizIdString, String.valueOf(mQuizScore));
                    return;
                } else {
                    try {
                        answersCount = response.body().getQuestions().get(question).getAnswers().size();

                        view.setQuizPlayName(response.body().getTitle());
                        view.setQuizQuestionName(response.body().getQuestions().get(question).getText());
                        view.setQuizImage(response.body().getQuestions().get(question).getImage().getUrl());

                        radioButton = new RadioButton[answersCount];
                        for (i1 = 0; i1 < answersCount; i1++) {
                            correctAnswerId = response.body().getQuestions().get(question).getAnswers().get(i1).getIsCorrect();
                            if (correctAnswerId != null) {
                                correctAnswerList.add(i1);
                            }

                            view.setupRadioButton(i1, response.body().getQuestions().get(question).getAnswers().get(i1).getText());
                            try {
                                Log.i(TAG, "onResponse: correctAnswerId: " + correctAnswerId);
                                if (radioButton[i1].isChecked()) {
                                    ansId = radioButton[i1].getId();
                                    userAnswerList.add(ansId);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "onResponse: " + e.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SingleQuizJsonResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
