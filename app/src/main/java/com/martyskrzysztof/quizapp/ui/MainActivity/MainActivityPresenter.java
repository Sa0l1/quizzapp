package com.martyskrzysztof.quizapp.ui.MainActivity;

import android.content.Context;
import android.util.Log;

import com.martyskrzysztof.quizapp.App;
import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.utils.database.AllQuizzesDatabaseHandler;
import com.martyskrzysztof.quizapp.utils.network.jsonResponses.AllQuizesJsonResponse;
import com.martyskrzysztof.quizapp.utils.network.RestAPI;
import com.martyskrzysztof.quizapp.utils.network.dataAdapters.AllQuizesDataAdapter;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Item;

import java.util.List;

import javax.inject.Inject;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by krzysztofm on 12.03.2018.
 */

public class MainActivityPresenter implements MainActivityMVP.Presenter {

    @Inject
    Retrofit retrofit;

    private static final String TAG = "MainActivityPresenter";

    private Context context;

    private MainActivityMVP.View view;

    public MainActivityPresenter(Context context, MainActivityMVP.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void loadRecyclerViewData() {
        Log.e(TAG, "loadRecyclerViewData: start");
        ((App) context.getApplicationContext()).getNetComponent().inject(this);

        RestAPI restApi = retrofit.create(RestAPI.class);
        Call<AllQuizesJsonResponse> call = restApi.getAllQuizes();
        call.enqueue(new Callback<AllQuizesJsonResponse>() {
            @Override
            public void onResponse(Call<AllQuizesJsonResponse> call, Response<AllQuizesJsonResponse> response) {
                List<Item> itemList = response.body().getItems();
                view.setAllQuizzesRecyclerViewAdapter(new AllQuizesDataAdapter(itemList, context, R.layout.quiz_row));
            }

            @Override
            public void onFailure(Call<AllQuizesJsonResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
