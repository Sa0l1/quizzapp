package com.martyskrzysztof.quizapp.ui.MainActivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.utils.network.dataAdapters.AllQuizesDataAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends Activity implements MainActivityMVP.View {

    private static final String TAG = "MainActivity";

    private MainActivityPresenter presenter;

    @BindView(R.id.rvAllQuizzes)
    RecyclerView rvAllQuizzes;

    private Unbinder unbind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbind = ButterKnife.bind(this);

        Log.e(TAG, "onCreate: start");

        presenter = new MainActivityPresenter(this, this);
        presenter.loadRecyclerViewData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    public void setAllQuizzesRecyclerViewAdapter(AllQuizesDataAdapter allQuizesDataAdapter) {
        rvAllQuizzes = findViewById(R.id.rvAllQuizzes);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvAllQuizzes.setHasFixedSize(true);
        rvAllQuizzes.setLayoutManager(layoutManager);

        rvAllQuizzes.setAdapter(allQuizesDataAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
