package com.martyskrzysztof.quizapp.ui.QuizFinishFragment;

import android.content.Context;

/**
 * Created by krzysztofm on 16.03.2018.
 */

public class QuizFinishFragmentPresenter implements QuizFinishFragmentMVP.Presenter {

    private static final String TAG = "QuizFinishFragmentPrese";

    private QuizFinishFragmentMVP.View view;
    private Context context;

    public QuizFinishFragmentPresenter(QuizFinishFragmentMVP.View view, Context context) {
        this.view = view;
        this.context = context;
    }
}
