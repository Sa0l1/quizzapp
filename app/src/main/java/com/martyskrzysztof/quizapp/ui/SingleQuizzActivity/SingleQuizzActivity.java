package com.martyskrzysztof.quizapp.ui.SingleQuizzActivity;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.ui.QuizzPlayFragment.QuizzPlayFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SingleQuizzActivity extends AppCompatActivity implements SingleQuizzActivityMVP.View {

    private static final String TAG = "SingleQuizzActivity";

    private SingleQuizzActivityPresenter presenter;

    private Unbinder unbind;

    private String mQuizIdString;

    @BindView(R.id.ivMainPhoto)
    ImageView ivMainPhoto;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvType)
    TextView tvType;

    @BindView(R.id.tvContent)
    TextView tvContent;

    @BindView(R.id.tvCreatedAt)
    TextView tvCreatedAt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_quizz);

        unbind = ButterKnife.bind(this);
        presenter = new SingleQuizzActivityPresenter(this, this);

        Bundle extras = getIntent().getExtras();
        if (extras.getString("SingleQuizzId") != null) {
            mQuizIdString = extras.getString("SingleQuizzId");
            presenter.loadSingleQuizzInfo(extras.getString("SingleQuizzId"));
        }else {
            onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                presenter.backPressedAlert();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        presenter.backPressedAlert();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    public void singleQuizzActivityBackPressed() {
        SingleQuizzActivity.super.onBackPressed();
    }

    @Override
    public void setMainPhotoUrl(String mainPhoto) {
        Glide.with(this)
                .load(mainPhoto)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter())
                .into(ivMainPhoto);
    }

    @Override
    public void setTitle(String quizTitleString) {
        tvTitle.setText(quizTitleString);
    }

    @Override
    public void setCreatedAt(Date quizCreatedAtString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.datetime_format));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        tvCreatedAt.setText(getString(R.string.dodano) + simpleDateFormat.format(quizCreatedAtString));
    }

    @Override
    public void setType(String quizTypeString) {
        tvType.setText("Typ: " + quizTypeString);
    }

    @Override
    public void setContent(String quizContentString) {
        tvContent.setText(quizContentString);
    }

    @OnClick(R.id.bStartQuiz)
    public void openQuizzPlayFragment() {
        Log.e(TAG, "openQuizzPlayFragment: start");

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, QuizzPlayFragment.newInstance(mQuizIdString));
        fragmentTransaction.commit();
    }
}
