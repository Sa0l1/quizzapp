package com.martyskrzysztof.quizapp.ui.MainActivity;

import com.martyskrzysztof.quizapp.utils.network.dataAdapters.AllQuizesDataAdapter;

/**
 * Created by krzysztofm on 12.03.2018.
 */

public interface MainActivityMVP {

    interface Presenter {
        void loadRecyclerViewData();
    }

    interface View {
        void setAllQuizzesRecyclerViewAdapter(AllQuizesDataAdapter allQuizesDataAdapter);
    }
}
