package com.martyskrzysztof.quizapp.ui.SingleQuizzActivity;

import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.martyskrzysztof.quizapp.App;
import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.ui.QuizzPlayFragment.QuizzPlayFragment;
import com.martyskrzysztof.quizapp.utils.network.RestAPI;
import com.martyskrzysztof.quizapp.utils.network.dataAdapters.AllQuizesDataAdapter;
import com.martyskrzysztof.quizapp.utils.network.jsonResponses.AllQuizesJsonResponse;
import com.martyskrzysztof.quizapp.utils.network.jsonResponses.SingleQuizJsonResponse;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Item;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by krzysztofm on 13.03.2018.
 */

public class SingleQuizzActivityPresenter implements SingleQuizzActivityMVP.Presenter {

    @Inject
    Retrofit retrofit;

    private static final String TAG = "SingleQuizzActivityPres";

    private SingleQuizzActivityMVP.View view;
    private Context context;

    public SingleQuizzActivityPresenter(SingleQuizzActivityMVP.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void backPressedAlert() {
        AlertDialog.Builder backPressedAlert = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        backPressedAlert.setTitle("Czy chcesz wyjść?");
        backPressedAlert.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                view.singleQuizzActivityBackPressed();
            }
        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        backPressedAlert.create().show();
    }

    @Override
    public void loadSingleQuizzInfo(String SingleQuizzId) {
        Log.e(TAG, "loadRecyclerViewData: start,odebrane SingleQuizzId: " + SingleQuizzId);
        ((App) context.getApplicationContext()).getNetComponent().inject(this);

        RestAPI restApi = retrofit.create(RestAPI.class);
        Call<SingleQuizJsonResponse> call = restApi.getSingleQuiz(SingleQuizzId);
        call.enqueue(new Callback<SingleQuizJsonResponse>() {
            @Override
            public void onResponse(Call<SingleQuizJsonResponse> call, Response<SingleQuizJsonResponse> response) {
                try {
                    String mainPhoto = response.body().getMainPhoto().getUrl();
                    if (!mainPhoto.isEmpty()) {
                        view.setMainPhotoUrl(mainPhoto);
                    }

                    String quizTitleString = response.body().getTitle();
                    if (!quizTitleString.isEmpty()) {

                        view.setTitle(quizTitleString);
                    }

                    Date quizCreatedAtString = response.body().getCreatedAt();
                    view.setCreatedAt(quizCreatedAtString);

                    String quizTypeString = response.body().getType();
                    if (!quizTypeString.isEmpty()) {

                        view.setType(quizTypeString);
                    }

                    String quizContentString = response.body().getContent();
                    view.setContent(quizContentString);
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SingleQuizJsonResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}