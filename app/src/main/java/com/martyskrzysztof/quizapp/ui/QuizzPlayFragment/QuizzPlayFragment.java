package com.martyskrzysztof.quizapp.ui.QuizzPlayFragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.martyskrzysztof.quizapp.App;
import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.ui.QuizFinishFragment.QuizFinishFragment;
import com.martyskrzysztof.quizapp.utils.network.RestAPI;
import com.martyskrzysztof.quizapp.utils.network.jsonResponses.SingleQuizJsonResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class QuizzPlayFragment extends Fragment implements QuizzPlayFragmentMVP.View {

    @Inject
    Retrofit retrofit;

    @BindView(R.id.tvQuestion)
    TextView tvQuestion;

    @BindView(R.id.tvQuizPlayName)
    TextView tvQuizPlayName;

    @BindView(R.id.bNextQuestion)
    Button bNextQuestion;

    @BindView(R.id.ivMainPhoto)
    ImageView ivMainPhoto;

    @BindView(R.id.answersRadioGroup)
    RadioGroup answersRadioGroup;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ArrayList<Integer> correctAnswerList;
    private ArrayList<Integer> userAnswerList;

    private static final String TAG = "QuizzPlayFragment";

    private static String QuizId;
    private static String mQuizIdString;
    private Integer correctAnswerId = 0;
    private int i = 0;
    private int i1 = 0;

    private Integer answersCount;
    private Integer mQuizScore = 0;

    private QuizzPlayFragmentPresenter presenter;
    private Unbinder unbind;

    private RadioButton[] radioButton;
    private int questionsCount;
    private int ansId;

    public QuizzPlayFragment() {
    }

    public static QuizzPlayFragment newInstance(String quizId) {
        QuizzPlayFragment fragment = new QuizzPlayFragment();

        Bundle args = new Bundle();
        args.putString(QuizId, quizId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mQuizIdString = getArguments().getString(QuizId);
            Log.e(TAG, "onCreate: odebralem mQuizIdString: " + mQuizIdString);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quizz_play, container, false);
        unbind = ButterKnife.bind(this, view);
        presenter = new QuizzPlayFragmentPresenter(getActivity(), this);

        userAnswerList = new ArrayList<>();
        correctAnswerList = new ArrayList<>();

        loadQuestionsData(mQuizIdString, i);
        return view;
    }

    @OnClick(R.id.bNextQuestion)
    public void onNextQuestionButtonClick() {
        i++;
        loadQuestionsData(mQuizIdString, i);

        answersRadioGroup.clearCheck();
        answersRadioGroup.removeAllViews();
    }

    private void loadQuestionsData(final String mQuizIdString, final int question) {
        ((App) getActivity().getApplicationContext()).getNetComponent().inject(this);

        RestAPI restApi = retrofit.create(RestAPI.class);
        Call<SingleQuizJsonResponse> call = restApi.getSingleQuiz(mQuizIdString);
        call.enqueue(new Callback<SingleQuizJsonResponse>() {
            @Override
            public void onResponse(Call<SingleQuizJsonResponse> call, Response<SingleQuizJsonResponse> response) {
                questionsCount = response.body().getQuestions().size();
                setProgressBarMaxSize(questionsCount);
                setProgressBarProgress(question);

                if (question >= questionsCount) {

                    for (int l = 0; l < userAnswerList.size(); l++) {
                        try {
                            if (userAnswerList.get(l).equals(correctAnswerList.get(l))) {
                                mQuizScore++;
                                Log.d(TAG, "onResponse: mQuizScore: " + mQuizScore);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                    openFinishFragment(mQuizIdString, String.valueOf(mQuizScore));
                    return;
                } else {
                    try {
                        answersCount = response.body().getQuestions().get(question).getAnswers().size();

                        setQuizPlayName(response.body().getTitle());
                        setQuizQuestionName(response.body().getQuestions().get(question).getText());
                        setQuizImage(response.body().getQuestions().get(question).getImage().getUrl());

                        radioButton = new RadioButton[answersCount];
                        for (i1 = 0; i1 < answersCount; i1++) {
                            correctAnswerId = response.body().getQuestions().get(question).getAnswers().get(i1).getIsCorrect();
                            if (correctAnswerId != null) {
                                correctAnswerList.add(i1);
                            }

                            setupRadioButton(i1, response.body().getQuestions().get(question).getAnswers().get(i1).getText());
                            try {
                                Log.i(TAG, "onResponse: correctAnswerId: " + correctAnswerId);
                                if (radioButton[i1].isChecked()) {
                                    ansId = radioButton[i1].getId();
                                    userAnswerList.add(ansId);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "onResponse: " + e.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SingleQuizJsonResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbind.unbind();
    }

    @Override
    public void openFinishFragment(String mQuizIdString, String mQuizScore) {
        Log.e(TAG, "openFinishFragment: mQuizScore: " + mQuizScore);
        Log.e(TAG, "openFinishFragment: mQuizIdString: " + mQuizIdString);

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, QuizFinishFragment.newInstance(mQuizIdString, mQuizScore));
        fragmentTransaction.commit();
    }

    @Override
    public void setupRadioButton(final int i1, String text) {
        Log.i(TAG, "setupRadioButton: ");
        answersRadioGroup.setOrientation(LinearLayout.VERTICAL);

        radioButton[i1] = new RadioButton(getActivity());
        answersRadioGroup.addView(radioButton[i1]);
        radioButton[i1].setText(text);
        radioButton[i1].setTextColor(Color.WHITE);
        radioButton[i1].setId(i1);
        radioButton[i1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userAnswerList.add(radioButton[i1].getId());
            }
        });
    }

    @Override
    public void setProgressBarMaxSize(int questionsCount) {
        progressBar.setMax(questionsCount);
    }

    @Override
    public void setProgressBarProgress(int i) {
        progressBar.setProgress(i);
    }

    @Override
    public void setQuizPlayName(String title) {
        tvQuizPlayName.setText(title);
    }

    @Override
    public void setQuizQuestionName(String text) {
        tvQuestion.setText(text);
    }

    @Override
    public void setQuizImage(String url) {
        Glide.with(getActivity())
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter())
                .into(ivMainPhoto);
    }

    @Override
    public int returnUserAnswerListSize() {
        return userAnswerList.size();
    }

    @Override
    public int returnUserAnswerListValues(int i) {
        return userAnswerList.get(i);
    }
}