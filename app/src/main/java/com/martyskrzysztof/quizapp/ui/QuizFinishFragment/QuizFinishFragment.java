package com.martyskrzysztof.quizapp.ui.QuizFinishFragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.ui.MainActivity.MainActivity;
import com.martyskrzysztof.quizapp.ui.SingleQuizzActivity.SingleQuizzActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class QuizFinishFragment extends Fragment implements QuizFinishFragmentMVP.View {

    private static final String TAG = "QuizFinishFragment";

    //temp
    private static String mQuizId;
    private static String mDoneQuizScore;

    private String mQuizIdString;
    private String mQuizScore;

    private QuizFinishFragmentPresenter presenter;

    private Unbinder unbinder;

    @BindView(R.id.tvAnswerScore)
    TextView tvAnswerScore;

    @BindView(R.id.bOpenSingleQuizFragment)
    Button bOpenSingleQuizFragment;

    @BindView(R.id.bOpenMainActivity)
    Button bOpenMainActivity;

    public QuizFinishFragment() {
        // Required empty public constructor
    }

    public static QuizFinishFragment newInstance(String mQuizIdString, String mQuizScore) {
        QuizFinishFragment fragment = new QuizFinishFragment();

        Bundle args = new Bundle();
        args.putString("mQuizId", mQuizIdString);
        args.putString("mDoneQuizScore", mQuizScore);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mQuizIdString = getArguments().getString("mQuizId");
            mQuizScore = getArguments().getString("mDoneQuizScore");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quiz_finish, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new QuizFinishFragmentPresenter(this, getActivity());

        tvAnswerScore.setText(mQuizScore);

        bOpenSingleQuizFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: mQuizIdString: " + mQuizIdString);
                onOpenPlayFragmentAgain(mQuizIdString);
            }
        });

        bOpenMainActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOpenAllQuizesButtonClick();
            }
        });

        return view;
    }

    @Override
    public void onOpenPlayFragmentAgain(String mQuizIdString) {
        Intent intent = new Intent(getActivity(), SingleQuizzActivity.class);
        intent.putExtra("SingleQuizzId", mQuizIdString);
        startActivity(intent);
    }

    @Override
    public void onOpenAllQuizesButtonClick() {
        startActivity(new Intent(getActivity(), MainActivity.class));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }
}
