package com.martyskrzysztof.quizapp.ui.QuizFinishFragment;

/**
 * Created by krzysztofm on 16.03.2018.
 */

interface QuizFinishFragmentMVP {
    interface View {
        void onOpenPlayFragmentAgain(String mQuizId);

        void onOpenAllQuizesButtonClick();
    }

    interface Presenter {
    }
}
