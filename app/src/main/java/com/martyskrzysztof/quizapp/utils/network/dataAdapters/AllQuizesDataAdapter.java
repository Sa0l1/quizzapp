package com.martyskrzysztof.quizapp.utils.network.dataAdapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.martyskrzysztof.quizapp.R;
import com.martyskrzysztof.quizapp.ui.SingleQuizzActivity.SingleQuizzActivity;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Item;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by krzysztofm on 12.03.2018.
 */

public class AllQuizesDataAdapter extends RecyclerView.Adapter<AllQuizesDataAdapter.ViewHolder> {

    private static final String TAG = "AllQuizesDataAdapter";

    private List<Item> quizzesList;
    private Context context;
    private int rowLayout;

    public AllQuizesDataAdapter(List<Item> quizzesList, Context context, int rowLayout) {
        this.quizzesList = quizzesList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quiz_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.tvTitle.setText(quizzesList.get(position).getTitle());

            Date createdAt = quizzesList.get(position).getCreatedAt();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(context.getString(R.string.datetime_format));
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

            holder.tvCreatedAt.setText(context.getString(R.string.dodano) + simpleDateFormat.format(createdAt));

            String type = quizzesList.get(position).getType();
            if (!type.isEmpty()) {
                holder.tvType.setText(context.getString(R.string.typ) + type);
            }

            String questionsCount = quizzesList.get(position).getQuestions().toString();
            if (!questionsCount.isEmpty()) {
                holder.tvQuestions.setText(context.getString(R.string.liczba_pytan) + questionsCount);
            }

            holder.tvContent.setText(quizzesList.get(position).getContent());

            Glide.with(context)
                    .load(quizzesList.get(position).getMainPhoto().getUrl())
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter())
                    .into(holder.ivMainPhoto);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), SingleQuizzActivity.class);
                    intent.putExtra("SingleQuizzId", "" + quizzesList.get(position).getId());
                    view.getContext().startActivity(intent);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "onBindViewHolder: " + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return quizzesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * info
         */
        private TextView tvCreatedAt;
        private TextView tvQuestions;
        private TextView tvTitle;
        private TextView tvType;
        private TextView tvContent;

        /**
         * photo
         */
        private ImageView ivMainPhoto;

        public ViewHolder(View itemView) {
            super(itemView);

            tvCreatedAt = itemView.findViewById(R.id.tvCreatedAt);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvQuestions = itemView.findViewById(R.id.tvQuestions);
            tvType = itemView.findViewById(R.id.tvType);
            tvContent = itemView.findViewById(R.id.tvContent);

            ivMainPhoto = itemView.findViewById(R.id.ivMainPhoto);
        }
    }
}