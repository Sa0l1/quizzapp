package com.martyskrzysztof.quizapp.utils.network;

import com.martyskrzysztof.quizapp.utils.network.jsonResponses.AllQuizesJsonResponse;
import com.martyskrzysztof.quizapp.utils.network.jsonResponses.SingleQuizJsonResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by krzysztofm on 12.03.2018.
 */

public interface RestAPI {
    @GET("quizzes/0/100")
    Call<AllQuizesJsonResponse> getAllQuizes();

    @GET("quiz/{id}/0")
    Call<SingleQuizJsonResponse> getSingleQuiz(@Path("id") String id);
}
