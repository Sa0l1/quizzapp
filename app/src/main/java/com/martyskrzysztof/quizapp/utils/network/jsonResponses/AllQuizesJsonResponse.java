package com.martyskrzysztof.quizapp.utils.network.jsonResponses;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Item;

import java.util.List;

/**
 * Created by krzysztofm on 12.03.2018.
 */

public class AllQuizesJsonResponse {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
