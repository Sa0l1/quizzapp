package com.martyskrzysztof.quizapp.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Category;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Category_;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Item;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.MainPhoto;
import com.martyskrzysztof.quizapp.utils.network.models.allQuizzes.Tag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krzysztofm on 14.03.2018.
 */

public class AllQuizzesDatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "AllQuizzesDatabaseHandl";

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "AllQuizzes";

    // table names
    private static final String TABLE_ITEMS = "items";
    private static final String TABLE_CATEGORIES = "categories";
    private static final String TABLE_MAIN_PHOTO = "main_photo";
    private static final String TABLE_CATEGORY_ = "category_";
    private static final String TABLE_TAGS = "tags";

    // items Table Columns names
    private static final String KEY_ITEMS_ID = "id";
    private static final String KEY_ITEMS_buttonStart = "buttonStart";
    private static final String KEY_ITEMS_shareTitle = "shareTitle";
    private static final String KEY_ITEMS_questions = "questions";
    private static final String KEY_ITEMS_createdAt = "createdAt";
    private static final String KEY_ITEMS_sponsored = "sponsored";
    private static final String KEY_ITEMS_title = "title";
    private static final String KEY_ITEMS_type = "type";
    private static final String KEY_ITEMS_content = "content";

    /**
     * foreign key
     */
    private static final String KEY_ITEMS_categoriesId_FK = "categoriesId";
    private static final String KEY_ITEMS_mainPhotoId_FK = "mainPhotoId";
    private static final String KEY_ITEMS_categoryId_FK = "categoryId";
    private static final String KEY_ITEMS_tagsId_FK = "tagsId";

    //categories Table Columns names
    private static final String KEY_CATEGORIES_UID = "uid";
    private static final String KEY_CATEGORIES_secondaryCid = "secondaryCid";
    private static final String KEY_CATEGORIES_name = "name";
    private static final String KEY_CATEGORIES_type = "type";

    //main photo Table Columns names
    private static final String KEY_MAIN_PHOTO_url = "url";
    private static final String KEY_MAIN_PHOTO_mediaId = "mediaId";
    private static final String KEY_MAIN_PHOTO_author = "author";
    private static final String KEY_MAIN_PHOTO_width = "width";
    private static final String KEY_MAIN_PHOTO_source = "source";
    private static final String KEY_MAIN_PHOTO_title = "title";
    private static final String KEY_MAIN_PHOTO_height = "height";

    //Category_ Table Columns names
    private static final String KEY_category__id = "id";
    private static final String KEY_category__name = "name";

    //tag Table Columns names
    private static final String KEY_TAG_uid = "uid";
    private static final String KEY_TAG_name = "name";
    private static final String KEY_TAG_type = "type";

    private static final String CREATE_TABLE_items = "CREATE TABLE "
            + TABLE_ITEMS + "(" + KEY_ITEMS_ID + " INTEGER PRIMARY KEY,"
            + KEY_ITEMS_buttonStart + " TEXT,"
            + KEY_ITEMS_shareTitle + " TEXT,"
            + KEY_ITEMS_questions + " TEXT,"
            + KEY_ITEMS_createdAt + " TEXT,"
            + KEY_ITEMS_sponsored + " TEXT,"
            + KEY_ITEMS_title + " TEXT,"
            + KEY_ITEMS_type + " TEXT,"
            /*+ KEY_ITEMS_categoriesId_FK + " TEXT,"
            + KEY_ITEMS_mainPhotoId_FK + " TEXT,"
            + KEY_ITEMS_categoryId_FK + " TEXT,"
            + KEY_ITEMS_tagsId_FK + " TEXT,"*/
            + KEY_ITEMS_content + " TEXT"
            + ")";

    private static final String CREATE_TABLE_categories = "CREATE TABLE "
            + TABLE_CATEGORIES + "(" + KEY_CATEGORIES_UID + " INTEGER PRIMARY KEY,"
            + KEY_CATEGORIES_secondaryCid + " TEXT,"
            + KEY_CATEGORIES_name + " TEXT,"
            + KEY_CATEGORIES_type + " TEXT"
            + ")";

    private static final String CREATE_TABLE_main_photo = "CREATE TABLE "
            + TABLE_MAIN_PHOTO + "(" + KEY_MAIN_PHOTO_mediaId + " INTEGER PRIMARY KEY,"
            + KEY_MAIN_PHOTO_url + " TEXT,"
            + KEY_MAIN_PHOTO_author + " TEXT,"
            + KEY_MAIN_PHOTO_width + " TEXT,"
            + KEY_MAIN_PHOTO_source + " TEXT,"
            + KEY_MAIN_PHOTO_title + " TEXT,"
            + KEY_MAIN_PHOTO_height + " TEXT"
            + ")";

    private static final String CREATE_TABLE_Category_ = "CREATE TABLE "
            + TABLE_CATEGORY_ + "(" + KEY_category__id + " INTEGER PRIMARY KEY,"
            + KEY_category__name + " TEXT"
            + ")";

    private static final String CREATE_TABLE_TAGS = "CREATE TABLE "
            + TABLE_TAGS + "(" + KEY_TAG_uid + " INTEGER PRIMARY KEY,"
            + KEY_TAG_name + " TEXT,"
            + KEY_TAG_type + " TEXT"
            + ")";

    public AllQuizzesDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_items);
        sqLiteDatabase.execSQL(CREATE_TABLE_categories);
        sqLiteDatabase.execSQL(CREATE_TABLE_main_photo);
        sqLiteDatabase.execSQL(CREATE_TABLE_Category_);
        sqLiteDatabase.execSQL(CREATE_TABLE_TAGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVer, int newVer) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MAIN_PHOTO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY_);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TAGS);

        onCreate(sqLiteDatabase);
    }

    public Item addQuizItem(Item item, Category category, MainPhoto mainPhoto, Category_ category_, Tag tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            /**
             * items
             */
            ContentValues values_items = new ContentValues();
            values_items.put(KEY_ITEMS_ID, item.getId());
            values_items.put(KEY_ITEMS_buttonStart, item.getButtonStart());
            values_items.put(KEY_ITEMS_shareTitle, item.getShareTitle());
            values_items.put(KEY_ITEMS_questions, item.getQuestions());
            //values_items.put(KEY_ITEMS_createdAt, item.getCreatedAt());
            values_items.put(KEY_ITEMS_sponsored, item.getSponsored());
            values_items.put(KEY_ITEMS_title, item.getTitle());
            values_items.put(KEY_ITEMS_type, item.getType());
            /*values.put(KEY_ITEMS_categoriesId_FK, item.getId());
            values.put(KEY_ITEMS_mainPhotoId_FK, item.getId());
            values.put(KEY_ITEMS_categoryId_FK, item.getId());
            values.put(KEY_ITEMS_tagsId_FK, item.getId());*/
            values_items.put(KEY_ITEMS_content, item.getContent());

            /**
             * categories
             */
            ContentValues values_categories = new ContentValues();
            values_categories.put(KEY_CATEGORIES_name, item.getId());
            values_categories.put(KEY_CATEGORIES_secondaryCid, category.getSecondaryCid());
            values_categories.put(KEY_CATEGORIES_name, category.getName());
            values_categories.put(KEY_CATEGORIES_name, category.getType());

            /**
             * main photo
             */
            ContentValues values_main_photo = new ContentValues();
            values_main_photo.put(KEY_MAIN_PHOTO_mediaId, item.getId());
            values_main_photo.put(KEY_MAIN_PHOTO_url, mainPhoto.getUrl());
            values_main_photo.put(KEY_MAIN_PHOTO_author, mainPhoto.getAuthor());
            values_main_photo.put(KEY_MAIN_PHOTO_width, mainPhoto.getWidth());
            values_main_photo.put(KEY_MAIN_PHOTO_source, mainPhoto.getSource());
            values_main_photo.put(KEY_MAIN_PHOTO_title, mainPhoto.getTitle());
            values_main_photo.put(KEY_MAIN_PHOTO_height, mainPhoto.getHeight());

            /**
             * Category_
             */
            ContentValues values_category_ = new ContentValues();
            values_category_.put(KEY_category__id, item.getId());
            values_category_.put(KEY_category__name, category_.getName());

            /**
             * tag
             */
            ContentValues values_tag = new ContentValues();
            values_tag.put(KEY_TAG_uid, item.getId());
            values_tag.put(KEY_TAG_name, tag.getName());
            values_tag.put(KEY_TAG_type, tag.getType());

            db.insert(TABLE_ITEMS, null, values_items);
            db.insert(TABLE_CATEGORIES, null, values_categories);
            db.insert(TABLE_MAIN_PHOTO, null, values_main_photo);
            db.insert(TABLE_CATEGORY_, null, values_category_);
            db.insert(TABLE_TAGS, null, values_tag);
        } catch (Exception e) {
            Log.e(TAG, "addQuizItem: " + e.getMessage());
        } finally {
            db.close();
        }

        return item;
    }

    // Getting All quizzes
    public List<Item> getAllQuizzes() {
        List<Item> itemList = new ArrayList<Item>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Item item = new Item();
                item.setId(Integer.parseInt(cursor.getString(0)));
                item.setButtonStart(cursor.getString(1));
                item.setShareTitle(cursor.getString(2));
                item.setQuestions(Integer.parseInt(cursor.getString(3)));
                //item.setCreatedAt(cursor.getString(4));
                //item.setSponsored(cursor.getString(5));
                item.setTitle(cursor.getString(6));
                item.setType(cursor.getString(7));
                item.setContent(cursor.getString(8));

                // Adding contact to list
                itemList.add(item);
            } while (cursor.moveToNext());
        }

        // return contact list
        return itemList;
    }
}
