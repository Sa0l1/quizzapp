package com.martyskrzysztof.quizapp.utils.di.components;

import com.martyskrzysztof.quizapp.ui.MainActivity.MainActivity;
import com.martyskrzysztof.quizapp.ui.MainActivity.MainActivityPresenter;
import com.martyskrzysztof.quizapp.ui.QuizzPlayFragment.QuizzPlayFragment;
import com.martyskrzysztof.quizapp.ui.QuizzPlayFragment.QuizzPlayFragmentPresenter;
import com.martyskrzysztof.quizapp.ui.SingleQuizzActivity.SingleQuizzActivityPresenter;
import com.martyskrzysztof.quizapp.utils.di.modules.AppModule;
import com.martyskrzysztof.quizapp.utils.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by krzysztofm on 12.03.2018.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {
    void inject(MainActivityPresenter presenter);

    void inject(QuizzPlayFragment fragment);

    void inject(SingleQuizzActivityPresenter presenter);

    void inject(QuizzPlayFragmentPresenter presenter);
}
