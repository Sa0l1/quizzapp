package com.martyskrzysztof.quizapp.utils.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by krzysztofm on 14.03.2018.
 */

public class AllQuizzesModel {
    /**
     * mainItem
     */
    private String shareTitle;
    private Integer questions;
    private String createdAt;
    private Boolean sponsored;

    /**
     * category
     */
    private String categoryName;
    private String categoryType;

}
