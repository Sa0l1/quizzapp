package com.martyskrzysztof.quizapp;

import android.app.Application;

import com.martyskrzysztof.quizapp.utils.di.components.DaggerNetworkComponent;
import com.martyskrzysztof.quizapp.utils.di.components.NetworkComponent;
import com.martyskrzysztof.quizapp.utils.di.modules.AppModule;
import com.martyskrzysztof.quizapp.utils.di.modules.NetworkModule;

/**
 * Created by krzysztofm on 12.03.2018.
 */

public class App extends Application {

    private NetworkComponent mNetworkComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mNetworkComponent = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule("http://quiz.o2.pl/api/v1/"))
                .build();
    }

    public NetworkComponent getNetComponent() {
        return mNetworkComponent;
    }
}
